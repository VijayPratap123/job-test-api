package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableAutoConfiguration(exclude = { JacksonAutoConfiguration.class,WebMvcAutoConfiguration.class  })

public class UnderTowApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnderTowApplication.class, args);
	}
	
}
