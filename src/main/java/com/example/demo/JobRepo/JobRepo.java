package com.example.demo.JobRepo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.pojo.Job;


@Repository
public interface JobRepo extends MongoRepository<Job, String> {

}
