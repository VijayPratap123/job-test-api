package com.example.demo.JobController;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.JobRepo.JobRepo;
import com.example.demo.pojo.Job;

@RestController
public class JobController extends BaseController {

	private static org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager
			.getLogger(JobController.class);

	@Autowired
	private JobRepo jobRepo;

	@RequestMapping(method = RequestMethod.POST, value = "/job")
	public ResponseEntity<?> postJob(@RequestBody Job job) {
		if (job == null)
			return newresponseNotAcceptable(logger, "job is null");
		Job createdJob = null;
		createdJob = jobRepo.save(job);
		if (createdJob != null) {
			String log = "Job has been successfully created";
			logger.info(log);
			return ResponseEntity.status(HttpStatus.OK).body(createdJob);
		}
		return newresponseUnauthorized(logger);

	}
	@RequestMapping(method = RequestMethod.GET, value = "/getJob/{jobId}")
	public ResponseEntity<?> getJob(@PathVariable("jobId") String jobId) {
	Optional<Job>	job=jobRepo.findById(jobId);
	if (job.isPresent()) {
		return ResponseEntity.status(HttpStatus.OK).body(job.get());
	}
		
			return newresponseNotAcceptable(logger, "job is null");
		
	}
	@RequestMapping(method = RequestMethod.GET, value = "/getAllJob")
	public ResponseEntity<?> getAllJob() {
	List<Job>	job=jobRepo.findAll();
	if (job != null) {
		return ResponseEntity.status(HttpStatus.OK).body(job);
	}
		
			return newresponseNotAcceptable(logger, "job is null");
		
	}
	
	
	
}